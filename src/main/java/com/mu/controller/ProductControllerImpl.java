package com.mu.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mu.entity.Employee;
import com.mu.service.ProductService;
import com.mu.service.ProductServiceImpl;

import io.swagger.annotations.ApiOperation;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@RestController
@EnableSwagger2
@ComponentScan(basePackages = { "com.mu.controller" })
public class ProductControllerImpl {

	@Autowired
	private ProductServiceImpl productServiceImpl;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@GetMapping(value = "/getAllEmployees")
	@ApiOperation(value = "Get Hello World", nickname = "GET Method", 
	notes = "This endpoint retrieves Hello World", produces = "application/json")
	public Iterable<Employee> getHelloWorld(@RequestHeader(value = "authToken", required = false) String authToken) throws Exception {
		logger.info("I'm in getHelloWorld() to return Hello World!");
		return productServiceImpl.findAll();
	}
}
